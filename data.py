import os
from constant import * 
import pandas as pd 
import pyterrier as pt
os.environ["JAVA_HOME"] = "/usr/lib/jvm/java-11-openjdk-amd64"
pt.init()
import numpy as np 
import config
pd.set_option('display.max_columns', None)
import pandas as pd
import torch.nn as nn
import torch 
import random

# parsing dataset 
def create_index(index_df, text_col, id_col, overwrite=False): 
    """
        creates an index given a dataframe
        Args: 
            index_df : a pandas dataframe 
            text_col : the column to access the content of the document 
            id_col : the column to access the id of the document
    """

    
    pd_indexer = pt.DFIndexer(config.index_path, overwrite=False, verbose=True)
    indexref = pd_indexer.index(index_df[text_col], index_df[id_col])
    return 


# specific to the qqp task
def create_qqp_index_df(train_df): 
    """
        extract questions1 and and questions2 with their respective ids
        and concatenate them to create a dataframe of all questions. 
        the returned columns are : 

        docno : question id
        text : question content
        relevant : relevant pair question
    """
    train1 = pd.DataFrame({
        'docno':train_df["qid1"].tolist(),
        'text':train_df["question1"].tolist(),
        'relevant' : train_df["qid2"]
    })
    train2 = pd.DataFrame({
        'docno':train_df["qid2"].tolist(),
        'text':train_df["question2"].tolist(),
        'relevant' : train_df["qid1"]
    })
    train_index = pd.concat([train1, train2])
    train_index["text"] = train_index["text"].astype(str)
    train_index["docno"] = train_index["docno"].astype(str)
    train_index.to_csv(config.index_df_path)
    return train_index
    

def tokenize_pair(q1_serie, q2_serie, tokenizer): 
    """
        q1_serie : A serie containing the first questions 
        q2_serie : A serie contianing the second questions
    """    
    assert q1_serie.shape == q2_serie.shape 
    m = len(q1_serie)
    encodings = []
    print(q1_serie.iloc[0])
    for i in range(m): 
        encodings.append(tokenizer(q1_serie.iloc[i], q2_serie.iloc[i], return_tensors='pt', padding=True))

    return encodings  




class QQPDataset(torch.utils.data.Dataset): 
    """
        takes a dataframe and creates a torch Dataset Object
    """
    def __init__(self, qqp_dataframe, index_df,tokenizer):
        self.dataframe = qqp_dataframe   
        self.index_df = index_df 
        self.bm25 = pt.BatchRetrieve(config.index_path, wmodel="BM25")
        self.tokenizer = tokenizer 

    
    def __len__(self): 
        return self.dataframe.shape[0]

    def __getitem__(self, idx):         
        d = {}
        d["question1"] = self.dataframe["question1"].iloc[idx] 
        d["question2"] = self.dataframe["question2"].iloc[idx] 
        qid2 = self.dataframe["qid2"].iloc[idx]
        qid1 = self.dataframe["qid1"].iloc[idx]
        # get top 10 bmf 25
        negative_samples = self._negative_sampling(d["question1"],qid1, qid2)
        tokens = []
        for i in range(len(negative_samples)): 
            token = self._tokenize(d["question1"], negative_samples["text"].iloc[i], self.tokenizer)["input_ids"]
            tokens.append(token.squeeze())     
        d["negative_token"] = tokens # a list 
        d["negative_score"] = negative_samples["score"].tolist()
        d["true_token"] = self._tokenize(d["question1"], d["question2"], self.tokenizer)["input_ids"]
        return d


    def _negative_sampling(self, query, query_id, true_id): 
        """
            Performs negative sampling
            Args: 
            query : The query
        """
        query = "".join([x if x.isalnum() else " " for x in query])
        res = self.bm25.search(query)

        res = res[["docno", "score"]]
        res["docno"] = res["docno"].astype(str)
        self.index_df["docno"] = self.index_df["docno"].astype(str)
        res = res.merge(self.index_df[["text", "docno"]], "inner", "docno")
        res = res[res["docno"] != query_id]
        res = res[res["docno"] !=  true_id]
        res["relevant"] = 0
        res[res["docno"] == query_id]["relevant"] = 1
        res = res.drop_duplicates()
        res = res.iloc[:config.nb_candidates]  
        return res


    def _tokenize(self, question1, question2, tokenizer): 
        return tokenizer(question1, question2,return_tensors='pt')


def collate_batch(batch):
  
    tokens, labels, scores = [], [], []
    for i, sample in enumerate(batch):
        # shuffling 
        temp_tokens = sample["negative_token"]
        temp_labels = [0] * len(temp_tokens)
        temp_scores = sample["negative_score"]
        temp_tokens.extend(sample["true_token"])
        temp_labels.extend([1])
        temp_scores.extend([1/config.nb_classes])

        print(f"found {len(temp_tokens)} tokens for sample {i}")
        print(f"found {len(temp_scores)} scores for sample {i}")
        print(f"found {len(temp_labels)} labels for sample {i}")

        tokens.extend(temp_tokens)
        labels.extend(temp_labels) 
        scores.extend(temp_scores)


    labels = torch.tensor(labels, dtype=torch.int64)
    scores = torch.tensor(scores, dtype=torch.float32)
    
    tokens = torch.nn.utils.rnn.pad_sequence(tokens, batch_first=True, padding_value=0)

    print("token shape : ", len(tokens))
    print("labels shape : ", len(labels))
    print("scores shape : ", len(scores))


    idx = torch.randperm(labels.shape[0])


    tokens = tokens[idx] 
    scores = scores[idx] 
    labels = labels[idx]
    



    return tokens.to(config.device), labels.to(config.device), scores.to(config.device)