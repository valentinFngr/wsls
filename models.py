from turtle import forward
from transformers import BertModel
from torch import nn as nn
from transformers import DataCollatorWithPadding, BertTokenizer
import torch 


# tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
# model = BertModel.from_pretrained("bert-base-uncased")

# encoded_input = tokenizer("bonjour", "madame", truncation=True)
# print()
# print(encoded_input)
# print(tokenizer.decode(encoded_input["input_ids"]))


class MonoBERT(nn.Module): 
    """
        Class implementation of https://arxiv.org/pdf/2010.06467.pdf
    """
    def __init__(self): 
        super(MonoBERT, self).__init__()
        self.base_model = BertModel.from_pretrained("bert-base-uncased")
        self.dropout = nn.Dropout(0.5)
        self.linear = nn.Linear(768, 1)
        self.sigmoid = nn.Sigmoid()

    def forward(self, x): 
        """
            classic BERT fine tuning
        """
        output = self.base_model(x)[0] 
        # extract the first token embedding 
        output = self.dropout(self.linear(output[:,0,:].view(-1,768)))
        return self.sigmoid(output)

class LabelSmoothing(nn.Module): 

    def __init__(self, epsilon): 
        super(LabelSmoothing, self).__init__()
        self.epsilon = epsilon 
        
    def forward(self, y, score): 
        """
            Args: 
                x : tensor 
                score : Either bm25 score or uniform class probability
            Output: 
                loss
        """

        q = (1 - self.epsilon) * y + self.epsilon * score
        return q 