import os 
import torch


torch.manual_seed(0)
device = torch.device("cuda", 0)
# path related stuff
data_dir = os.path.join(os.getcwd(), "dataset")
index_path = os.path.join(os.getcwd(), "qqp_index")
train_path = os.path.join(data_dir, "train.csv")
validation_path = os.path.join(data_dir, "val.csv")
test_path = os.path.join(data_dir, "test.csv")
index_df_path = os.path.join(data_dir, "index_df.csv")
submission_path = os.path.join(data_dir, "sample_submission.csv")

# pickle 
train_df_path = os.path.join(data_dir, "train_df.pickle")
val_df_path = os.path.join(data_dir, "val_df.pickle")

#encoding
train_encoding_path = os.path.join(os.getcwd(), "encoding/train_encoding.pickle")
val_encoding_path = os.path.join(os.getcwd(), "encoding/val_encoding.pickle")


nb_classes = 2
validation_size = 0.2
lr = 5e-6 
espilon = 1e-8
batch_size = 2
nb_train_instances = 50000 
nb_candidates = 10 
epochs = 20
relevant_label = 1.0
irrelevant_label = 0.0



# exp 
experiment_name = "./runs/default_experiment"