from asyncore import write
import numpy as np 
import pandas as pd
from torch import negative 
import config
from data import * 
import os
from models import MonoBERT, LabelSmoothing
from transformers import BertTokenizer
from torch.utils.data import DataLoader
import pickle 
from torch.utils.tensorboard import SummaryWriter

#### TODO : SPLIT THE TRAIN DATASET IN TWO ! 


def get_dataframe(): 

    df = pd.read_csv(config.train_path, error_bad_lines=False, engine="python")
    # shuffle dataset 
    df.sample(frac=1)
    # remove nan 
    df = df.dropna()
    # splitting
    val_size = int(df.shape[0] * config.validation_size)
    val_df = df.iloc[:val_size]
    train_df = df.iloc[val_size:]
    print("--- Storing dataframes as pickled objects ---")
    train_df.to_pickle(config.train_df_path)
    val_df.to_pickle(config.val_df_path)
    print("Storing dataframe as pickled objects : DONE ", "\n")
    return train_df, val_df


def get_dataset(train_df, val_df, index_df, tokenizer):

    train_data = QQPDataset(train_df, index_df, tokenizer)
    val_data = QQPDataset(val_df, index_df, tokenizer)

    train_loader = DataLoader(
        train_data, 
        batch_size=config.batch_size, 
        shuffle=True,
        drop_last=True, 
        collate_fn=collate_batch

    )

    val_loader = DataLoader(
        val_data, 
        batch_size=config.batch_size, 
        shuffle=True,
        drop_last=True, 
        collate_fn=collate_batch
    )

    return train_loader, val_loader


def get_model_and_tokenerizer(): 
    """
        return monoBERT and its tokenizer
    """
    monoBERT = MonoBERT().to(config.device)
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
    return monoBERT, tokenizer



def get_optimizer(monoBERT): 
    optim = torch.optim.Adam(monoBERT.parameters(), lr=config.lr)
    return optim


def train(monoBERT, train_loader, val_loader, optimizer, label_smoothing, bce_criterion, writer, epoch): 

    for i, data in enumerate(train_loader): 

        tokens, labels, scores = data

        monoBERT.zero_grad()
        batch_size = tokens.shape[0]
        preds = monoBERT(tokens).squeeze().to(torch.float32)
        smoothed_labels = label_smoothing(labels, scores).detach() 
        
        loss = bce_criterion(preds, smoothed_labels)
        loss.backward()

        _preds = torch.where(preds >= 0.5, 1, 0).cpu().numpy() 
        _labels = labels.detach().cpu().numpy()
        
        # print(_preds) 
        # print(_labels)

        accuracy = (_preds == _labels).mean() 
        optimizer.step()
        # write to tensorboard
        writer.add_scalar("Training/Loss", loss.item(), epoch*i + i)
        writer.add_scalar("Training/Accuracy", accuracy, epoch*i + i)

        if i % 50 == 0: 
            
            print(f"EPOCH={epoch} | TRAIN_LOSS={loss.item()} | ACC={accuracy*100} % ")


def get_cirterions(): 
    criterion = torch.nn.BCELoss().to(config.device)
    label_smoothing = LabelSmoothing(config.espilon).to(config.device)
    return criterion, label_smoothing


def main(): 

    # extract dataframes

    # check if dataframe exists on disk for train and val 
    if os.path.exists(config.train_df_path) and os.path.exists(config.train_df_path): 
        print("--- loading train df from pickle ---")
        train_df = pd.read_pickle(config.train_df_path) 
        print("--- loading train df from pickle --- : DONE", "\n")
        print("--- loading val df from pickle ---")
        val_df = pd.read_pickle(config.val_df_path) 
        print("--- loading val df from pickle --- : DONE", "\n")

    else: 
        train_df, val_df = get_dataframe()

    print("val df shape : ", val_df.shape)
    print("train df shape : ", train_df.shape)
    

    # create index or load 
    if not os.path.exists(config.index_df_path): 
        print("--- creating index df ---")
        index_df = create_qqp_index_df(train_df)
        create_index(index_df, "text", "docno")
        print("--- Creating index : DONE & stored ---", "\n")
    else: 
        print("--- Index data already exist ---")
        index_df = pd.read_csv(config.index_df_path)
    
    print("--- Loading monoBERT ---")
    monoBERT, tokenizer = get_model_and_tokenerizer()
    print("--- Loading monoBERT : DONE ---", "\n")

    print("--- Loading optimzier --- ")
    optim = get_optimizer(monoBERT)
    print("--- Loading optimizer --- : DONE ", "\n")

    print("--- loading dataset ---")
    train_loader, val_loader = get_dataset(train_df, val_df, index_df, tokenizer)
    print("--- loading dataset : DONE ---", "\n")

    print("--- Get criterions ---") 
    bce_criterion, label_smoothing = get_cirterions()
    print("--- Get criterions : DONE ---") 

    writer = SummaryWriter(config.experiment_name)
    # training 

    for epoch in range(config.epochs): 
        train(monoBERT, train_loader, val_loader, optim, label_smoothing, bce_criterion, writer, epoch)

    


if __name__ == "__main__": 
    main()